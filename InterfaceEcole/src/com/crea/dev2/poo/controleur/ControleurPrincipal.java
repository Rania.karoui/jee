package com.crea.dev2.poo.controleur;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crea.dev2.poo.beans.Eleve;
import com.crea.dev2.poo.dao.EleveDAO;

/**
 * Servlet implementation class ControleurPrincipal
 */
@WebServlet("/ControleurPrincipal")
public class ControleurPrincipal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControleurPrincipal() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// this.getServletContext().getRequestDispatcher("/WEB-INF/errorPage.jsp").forward(request, response);

		String idform = request.getParameter("idaction"); // a partir de chaque formulaire HTML/XHTML/JSTL/JSP

		if (idform.equals("getelevebynum")) {

			String numelev = request.getParameter("numelev");
			try {
				ArrayList<Eleve> listEleve = EleveDAO.getAllEleve();

				Eleve erecup = EleveDAO.getEleveByNum(numelev);
				erecup.toString();
				System.out.print("list " + erecup.getNom());
				EleveDAO eleve = new EleveDAO();
				request.setAttribute("listEleve", eleve.getAllEleve());
				request.setAttribute("num", erecup.getNum());
				request.setAttribute("nom", erecup.getNom());
				request.setAttribute("age", erecup.getAge());
				request.setAttribute("adresse", erecup.getAdresse());

				if (erecup.getNum().equals(numelev)) {
					request.setAttribute("txtEnter", numelev);
					this.getServletContext().getRequestDispatcher("/WEB-INF/showStudent.jsp").forward(request,
							response);
				}

				else {
					request.setAttribute("message", numelev + " doesn't exist, try again.");
					this.getServletContext().getRequestDispatcher("/WEB-INF/listStudent.jsp").forward(request,
							response);
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (idform.equals("listStudent")) {
			try {
				EleveDAO eleve = new EleveDAO();
				request.setAttribute("listEleve", eleve.getAllEleve());
				this.getServletContext().getRequestDispatcher("/WEB-INF/listStudent.jsp").forward(request, response);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		else if (idform.equals("addEleve")) {
			String confirmationMsg = "Error";
			String num = request.getParameter("numElev");

			if (num.length() == 5) {
				String nom = request.getParameter("nomElev");
				int age = Integer.parseInt(request.getParameter("ageElev"));
				String adresse = request.getParameter("adresseElev");
				int code = EleveDAO.addEleve(num, nom, age, adresse);
				System.out.println("Code de l'operation : " + code);

				if (code == 1) {
					confirmationMsg = "Student added";
				}

				request.setAttribute("dataEnter", confirmationMsg);
				this.getServletContext().getRequestDispatcher("/WEB-INF/studentAdded.jsp").forward(request, response);
			} else {
				request.setAttribute("txtError", "5 letters please");
				this.getServletContext().getRequestDispatcher("/WEB-INF/addStudent.jsp").forward(request, response);
			}

		}

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
