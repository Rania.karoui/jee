<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@page import="java.util.ArrayList"%> <%@page
import="java.util.List"%> <%@page import="com.crea.dev2.poo.dao.EleveDAO"%>
<%@page import="com.crea.dev2.poo.beans.Eleve"%> <%@page
import="com.crea.dev2.poo.controleur.ControleurPrincipal"%> <%@taglib
uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
      crossorigin="anonymous"
    />
    <title>Home Page</title>
  </head>
  <style>
    .position-top {
      right: 20px;
      top: 20px;
    }
    .pgtitre {
      margin: 100px 0px 50px;
    }
    .btn {
      width: 200px;
      margin: 20px;
    }
    .logo-home {
      width: 50px;
      margin: 20px 50px;
    }
  </style>
  <body>
    <a href="/InterfaceEcole">
      <img
        class="logo-home"
        src="https://image.flaticon.com/icons/svg/2948/2948176.svg"
        alt="homePage"
      />
    </a>

    <div align="center" class="m-5">
      <form
        name="id"
        class="form position-absolute position-top"
        action="ControleurPrincipal?idaction=getelevebynum"
        method="POST"
      >
        <% String message = (String) request.getAttribute("message");
        if(message!=null){ %>
        <div class="alert alert-danger" role="alert">
          <% out.print(message); %>
        </div>
        <% } %>
        <table>
          <tr>
            <td>Search student by number :</td>
            <td>
              <input
                class="border-info ml-3"
                type="text"
                name="numelev"
                id="numelev"
                placeholder="eg: AGUE001"
              />
            </td>

            <td width="120px">
              <input type="submit" value="Search" class="submit bg-info" />
            </td>
          </tr>
        </table>
      </form>

      <h1 class="pgtitre">List of students</h1>

      <table class="table">
        <thead>
          <tr class="table-info">
            <th scope="col">number</th>
            <th scope="col">name</th>
            <th scope="col">age</th>
            <th scope="col">address</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="elevTemp" items="${listEleve}">
            <tr>
              <td><c:out value="${ elevTemp.num }" /></td>
              <td><c:out value="${ elevTemp.nom }" /></td>
              <td><c:out value="${ elevTemp.age }" /></td>
              <td><c:out value="${ elevTemp.adresse }" /></td>
            </tr>
          </c:forEach>
        </tbody>
      </table>

      <button
        type="button"
        class="btn btn-danger"
        name="back"
        onclick="history.back()"
      >
        back
      </button>
    </div>
  </body>
</html>
