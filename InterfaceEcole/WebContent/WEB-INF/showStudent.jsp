<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
      crossorigin="anonymous"
    />

    <title>Page Show</title>
  </head>

  <style>
    .pgtitre {
      margin: 100px 0px 50px;
    }
    .btn {
      width: 200px;
      margin: 20px;
    }
    .logo-home {
      width: 50px;
      margin: 20px 50px;
    }
  </style>

  <body>
    <a href="/InterfaceEcole">
      <img
        class="logo-home"
        src="https://image.flaticon.com/icons/svg/2948/2948176.svg"
        alt="homePage"
      />
    </a>

    <div align="center" class="m-5">
      <% String txtEnter = (String) request.getAttribute("txtEnter"); %>
      <h1 class="pgtitre">Student n° <% out.println(txtEnter); %></h1>

      <table class="table">
        <thead>
          <tr class="table-info">
            <th scope="col">number</th>
            <th scope="col">name</th>
            <th scope="col">age</th>
            <th scope="col">address</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <% String num = (String) request.getAttribute("num");
              out.println(num); %>
            </td>

            <td>
              <% String nom = (String) request.getAttribute("nom");
              out.println(nom); %>
            </td>
            <td>
              <% Integer age = (Integer) request.getAttribute("age");
              out.println(age); %>
            </td>
            <td>
              <% String adresse = (String) request.getAttribute("adresse");
              out.println(adresse); %>
            </td>
          </tr>
        </tbody>
      </table>
      <button
        type="button"
        class="btn btn-danger"
        name="back"
        onclick="history.back()"
      >
        back
      </button>
    </div>
  </body>
</html>
