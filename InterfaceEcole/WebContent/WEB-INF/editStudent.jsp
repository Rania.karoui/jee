<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Edit student</title>
  </head>
  <style>
    .pgtitre {
      margin: 100px 0px 50px;
    }
    .btn {
      width: 200px;
      margin: 20px;
    }
    .logo-home {
      width: 50px;
      margin: 20px 50px;
    }
  </style>

  <body>
    <a href="/InterfaceEcole">
      <img
        class="logo-home"
        src="https://image.flaticon.com/icons/svg/2948/2948176.svg"
        alt="homePage"
      />
    </a>

    <div align="center">
      <h1 class="pgtitre">Edit student</h1>
      <form
        name="id"
        class="form"
        action="ControleurPrincipal?idaction=updAdresseEleveByNum"
        method="POST"
      >
        <table class="table">
          <tr class="table-info">
            <td width="120px">Student Number :</td>
            <td>
              <input type="text" name="numElev" id="numElev" value="numElev" />
            </td>

            <td width="120px">Student name :</td>
            <td><input type="text" name="nomElev" id="nomElev" /></td>

            <td width="120px">Age :</td>
            <td><input type="text" name="ageElev" id="ageElev" /></td>

            <td width="120px">Adresse :</td>
            <td><input type="text" name="adresseElev" id="adresseElev" /></td>

            <td width="120px">
              <input type="submit" value="Valider" class="submit" />
            </td>
          </tr>
        </table>
      </form>
      <button
        type="button"
        class="btn btn-danger"
        name="back"
        onclick="history.back()"
      >
        back
      </button>
    </div>
  </body>
</html>
