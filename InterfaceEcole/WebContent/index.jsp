<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
      crossorigin="anonymous"
    />
    <title>Home Page</title>
  </head>
  <style>
    .pgtitre {
      margin: 100px 0px 50px;
    }

    .btn {
      width: 200px;
      margin: 20px;
      display: inline;
      padding: 20px;
    }

    .content-flex {
      display: flex;
      justify-content: center;
    }
  </style>
  <body>
    <div align="center" class="m-5">
      <h1 class="pgtitre">Welcome to LiliSchool</h1>
      <img
        width="500"
        src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flaforetleroi.files.wordpress.com%2F2008%2F08%2Fecole-la-foret-le-roi-1.jpg&f=1&nofb=1"
        alt="liliSchool"
      />

      <div class="content-flex">
        <form
          name="id"
          action="ControleurPrincipal?idaction=listStudent"
          method="POST"
        >
          <input
            type="submit"
            value="List of students"
            class="submit btn bg-info"
          />
        </form>
        <form
          name="id"
          action="ControleurPrincipal?idaction=addEleve"
          method="POST"
        >
          <input type="submit" value="add student" class="submit btn bg-info" />
        </form>
      </div>
    </div>
  </body>
</html>
