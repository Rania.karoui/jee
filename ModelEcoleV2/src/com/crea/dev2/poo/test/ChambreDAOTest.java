package com.crea.dev2.poo.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.crea.dev2.poo.beans.Chambre;
import com.crea.dev2.poo.dao.ChambreDAO;

public class ChambreDAOTest {

	@Before
	public void testAddListeLivreToTest() {
		int insert = 1;
		
		assertEquals(insert, ChambreDAO.addChambre(234, "AGUE001", (float) 230.30));
		assertEquals(insert, ChambreDAO.addChambre(24, "AGUE002", (float) 150.80));
		assertEquals(insert, ChambreDAO.addChambre(71, "KAMTO005", (float) 70.20));
		assertEquals(insert, ChambreDAO.addChambre(42, "TABIS003", (float) 212.10));
	}

	@After
	public void testDeleteListeLivreToTest() {
		int delete = 1;
		assertEquals(delete, ChambreDAO.deleteChambreByNo(234));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(24));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(71));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(42));
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetChambreByNo() throws SQLException {
		Chambre e_ref = new Chambre(12, "AGUE001", (float) 92.80);
		Chambre e_res = new Chambre();
		e_res = ChambreDAO.getChambreByNo(12);

		assertEquals(e_ref.getNo(), e_res.getNo());
		assertEquals(e_ref.getPrix(), e_res.getPrix());
		assertEquals(e_ref.getNum(), e_res.getNum());
	}

	@Test
	public void testDeleteChambreByNo() {
		int insert = 1;
		int delete = 1;
		assertEquals(insert, ChambreDAO.addChambre(1, "AGUE001", (float) 200.30));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(1));
	}

	@Test
	public void testUpdPrixChambreByNo() {
		int update = 1;
		assertEquals(update, ChambreDAO.updPrixChambreByNo(12, (float) 300.55));
	}

	@Test
	public void testAddChambre() {
		int insert = 1;
		int delete = 1;
		assertEquals(insert, ChambreDAO.addChambre(234, "AGUE001", (float) 230.30));
		assertNotEquals(insert, ChambreDAO.addChambre(24, "AGUE002", (float) 180.90));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(234));
	}

	@Test
	public void testGetAllChambre() throws SQLException {
		ArrayList<Chambre> listChambre = new ArrayList<Chambre>();

		Chambre e1 = new Chambre(234, "AGUE001", (float) 230.30);
		Chambre e2 = new Chambre(24, "AGUE002", (float) 150.80);
		Chambre e3 = new Chambre(42, "TABIS003", (float) 212.10);
		Chambre e4 = new Chambre(71, "KAMTO005", (float) 70.20);


		listChambre.add(e1);
		listChambre.add(e2);
		listChambre.add(e3);
		listChambre.add(e4);

		for (int i = 0; i < listChambre.size(); i++) {
			assertEquals(listChambre.get(i).getNo(), ChambreDAO.getAllChambre().get(i).getNo());
			assertEquals(listChambre.get(i).getPrix(), ChambreDAO.getAllChambre().get(i).getPrix());
			assertEquals(listChambre.get(i).getNum(), ChambreDAO.getAllChambre().get(i).getNum());
		}
	}

}
