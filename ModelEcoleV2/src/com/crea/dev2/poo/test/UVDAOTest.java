package com.crea.dev2.poo.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.crea.dev2.poo.beans.UV;
import com.crea.dev2.poo.dao.UVDAO;

public class UVDAOTest {

	@Test
	public void testGetUVByCode() throws SQLException {
		UV e_ref = new UV("12vfr", 3, "CREA", "AGUE001");
		UV e_res = new UV();
		e_res = UVDAO.getUVByCode("12vfr");

		assertEquals(e_ref.getCode(), e_res.getCode());
		assertEquals(e_ref.getNum(), e_res.getNum());
		assertEquals(e_ref.getCoord(), e_res.getCoord());
		assertEquals(e_ref.getNbh(), e_res.getNbh());

	}

}
