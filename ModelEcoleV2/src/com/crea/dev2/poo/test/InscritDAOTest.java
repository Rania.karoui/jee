package com.crea.dev2.poo.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.crea.dev2.poo.beans.Inscrit;
import com.crea.dev2.poo.dao.InscritDAO;

public class InscritDAOTest {

	@Test
	public void testGetInscritByCode() throws SQLException {
		Inscrit e_ref = new Inscrit("EC23", "AGUE001", (float) 5.75);
		Inscrit e_res = new Inscrit();
		e_res = InscritDAO.getInscritByCode("EC23");

		assertEquals(e_ref.getCode(), e_res.getCode());
		assertEquals(e_ref.getNote(), e_res.getNote());
		assertEquals(e_ref.getNum(), e_res.getNum());

	}
}
