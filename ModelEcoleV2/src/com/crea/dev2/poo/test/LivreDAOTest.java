package com.crea.dev2.poo.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import com.crea.dev2.poo.beans.Chambre;
import com.crea.dev2.poo.beans.Livre;
import com.crea.dev2.poo.dao.ChambreDAO;
import com.crea.dev2.poo.dao.LivreDAO;

public class LivreDAOTest {

	@Test
	public void testGetLivreByCote() throws SQLException {
		Livre e_ref = new Livre("C27732", "AGUE001", "JEE pour les nuls", null);
		Livre e_res = new Livre();
		e_res = LivreDAO.getLivreByCote("C27732");

		assertEquals(e_ref.getCote(), e_res.getCote());
		assertEquals(e_ref.getDatepret(), e_res.getDatepret());
		assertEquals(e_ref.getTitre(), e_res.getTitre());
		assertEquals(e_ref.getNum(), e_res.getNum());

	}

	@Test
	public void testDeleteLivreByCote() {
		int insert = 1;
		int delete = 1;
		assertEquals(insert, LivreDAO.addLivre("5566b", "AGUE001", "C++ pour les nuls", null));
		assertEquals(delete, LivreDAO.deleteLivreByCote("5566b"));
	}


	@Test
	public void testAddLivre() {
		int insert = 1;
		int delete = 1;
		assertEquals(insert, LivreDAO.addLivre("bh677", "AGUE002", "Symfony pour les nuls", null));
		assertNotEquals(insert, LivreDAO.addLivre("5566b", "TABIS003", "Laravel pour les nuls", null));
		assertEquals(delete, LivreDAO.deleteLivreByCote("bh677"));
	}
	
	@Test
	public void testUpdTitreLivreByCote() {
		int update = 1;
		assertEquals(update, LivreDAO.updTitreLivreByCote("C27732", "Java pour les nuls"));
	}

	@Test
	public void testGetAllLivre() throws SQLException {
		ArrayList<Livre> listLivre = new ArrayList<Livre>();

		Livre l1 = new Livre("tgd67", "TABIS003", "React pour les nuls", null);
		Livre l2 = new Livre("fgg62", "AGUE002", "Wordpress pour les nuls", null);
		Livre l3 = new Livre("oop9", "KAMTO005", "Html pour les nuls", null);

		listLivre.add(l1);
		listLivre.add(l2);
		listLivre.add(l3);

		for (int i = 0; i < listLivre.size(); i++) {
			assertEquals(listLivre.get(i).getCote(), LivreDAO.getAllLivre().get(i).getCote());
			assertEquals(listLivre.get(i).getDatepret(), LivreDAO.getAllLivre().get(i).getDatepret());
			assertEquals(listLivre.get(i).getTitre(), LivreDAO.getAllLivre().get(i).getTitre());
			assertEquals(listLivre.get(i).getNum(), LivreDAO.getAllLivre().get(i).getNum());
		}
	}

}
