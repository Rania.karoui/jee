package com.crea.dev2.poo.dao;

//TO DO
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.imageio.IIOException;

import com.crea.dev2.poo.beans.Eleve;
import com.crea.dev2.poo.utils.DBAction;

public class EleveDAO {
	/**
	 * Liste un �l�ve en particulier d'apr�s son identifiant pass� en param�tre de
	 * la fonction.
	 * 
	 * @param num
	 * @return : retourne l'objet ElevTemp
	 * @throws SQLException
	 */
	public static Eleve getEleveByNum(String num) throws SQLException {
		Eleve elevTemp = new Eleve();
		// ResultSet res ;
		String req = "SELECT num, age, nom, no, adresse FROM eleve WHERE num = '" + num + "' ";
		// Connexion
		DBAction.DBConnexion();
		// exécution de la requete 
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			// Creation de l'objet eleveTemp à travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setAge(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setNo(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			elevTemp.affiche();
		}
		// Fermeture de la connexion & statement & res
		// res.close();
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return elevTemp;
	}

	/**
	 * Selection d'Eleve(s) par le nom
	 * 
	 * @param listsnoms
	 * @return :un liste d'eleve
	 */
	public static ArrayList<Eleve> getEleveByNom(String listsnoms) throws SQLException {
		// Création de ma liste d'élève ayant le meme nom
		ArrayList<Eleve> listEleveNom = new ArrayList<Eleve>();
		String req = "SELECT * FROM eleve WHERE nom = '" + listsnoms + "' ";

		// Connexion
		DBAction.DBConnexion();// System.out.println(req);
		// Exécution de la requete 
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		// Creation de l'objet eleveTemp à travers le ResultSet BD
		while (DBAction.getRes().next()) {
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp à travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setAge(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setNo(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleveNom.add(elevTemp);
		}
		for (int i = 0; i < listEleveNom.size(); i++) {
			listEleveNom.get(i).affiche();
		}
		// Fermeture de la connexion
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return listEleveNom;
	}

	/**
	 * Selection d'1 ou +sieurs Eleve par son num�ro de chambre
	 * 
	 * @param no
	 * @return :un objet eleve
	 */
	public static ArrayList<Eleve> getEleveByNo(int no) throws SQLException {
		// Création de ma liste d'élève partageant la même chambre
		ArrayList<Eleve> listEleveNo = new ArrayList<Eleve>();

		String req = "SELECT * FROM eleve where no = " + no + " ";
		// Connexion
		DBAction.DBConnexion();

		// Exécution de la requete 
		DBAction.setRes(DBAction.getStm().executeQuery(req));

		// Creation de l'objet eleveTemp à travers le ResultSet BD
		while (DBAction.getRes().next()) {
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp à travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setAge(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setNo(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleveNo.add(elevTemp);
			
		}

		// Fermeture de la connexion
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return listEleveNo;
	}

	/**
	 * Delete Eleve par un num�ro
	 * 
	 * @param num
	 * @return : 1 ou 0 (le nbr d'�tudiants supprim�s) sinon le (-) code d'erreur
	 */
	public static int deleteEleveByNum(String num) {
		int result = -1;
		DBAction.DBConnexion();
		String req = "DELETE FROM eleve WHERE num = '" + num + "' ";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executée");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		System.out.println("[" + req + "] Suppression : Valeur de result == " + result);
		DBAction.DBClose();
		return result;
	}

	/**
	 * Met � jour de l'adresse d'un �l�ve par son num�ro
	 * 
	 * @param num
	 * @return : 1 ou 0 (le nbr d'�tudiants mis � jour) sinon le (-) code d'erreur
	 */
	public static int updAdresseEleveByNum(String num, String adresse) throws SQLException {
		int result = -1;
		DBAction.DBConnexion();
		String req = "UPDATE eleve SET adresse = '" + adresse + "' WHERE num ='" + num + "' ";
		result = DBAction.getStm().executeUpdate(req);
		System.out.println("Requete executee");

		DBAction.DBClose();
		return result;
	}

	/**
	 * Met à jour(Attribuer une chambre à 1 élève) le no de chambre à un éléve
	 * 
	 * @param num
	 * @return : 1 ou 0 (le nbr d'étudiants mis à jour) sinon le (-) code d'erreur
	 */
	public static int updNoChambreEleveByNum(String num, int no) {
		int result = -1;
		DBAction.DBConnexion();

		String req = "UPDATE eleve SET no = " + no + " WHERE num ='" + num + "' ";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requete executee");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
		}
		DBAction.DBClose();
		return result;
	}

	/**
	 * Ajouter un élève
	 * 
	 * @param String num, String nom, int age, String adresse
	 * @return : 1 ou 0 (le nbr d'étudiants ajoutés) sinon le (-) code d'erreur
	 */
	public static int addEleve(String num, String nom, int age, String adresse) {
		int result = -1;
		DBAction.DBConnexion();

		String req = "INSERT INTO eleve (num, age, nom, no, adresse)" + " VALUES ('" + num + "',NULL,'" + nom + "',"
				+ age + ",'" + adresse + "') ";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		// System.out.println("["+req+"] Valeur de result == "+result);
		// System.out.println(req);
		DBAction.DBClose();
		return result;
	}

	/**
	 * Renvoie une liste d'�l�ves en fonction de l'age.
	 * 
	 * @param anneeNaissance : Date de naissance de(s) l'�l�ve(s)(l'ann�e)
	 * @return : 1 liste d'�l�ve ayant le m�me age.
	 */
	public static ArrayList<Eleve> getListEleveByDateN(int anneeNaissance) throws SQLException {
		ArrayList<Eleve> listEleveAnneeNaissance = new ArrayList<Eleve>();
		// On recupere l'année en cours
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);

		// Variable � passer en param�tre dans la requ�te pour avoir les �l�ves d'une
		// tranche d'age:
		// Je fais la diff�rence entre l'ann�e en cours et l'ann�e pass�e en param�tre
		// de la fonction.
		int anneeNaissanceEleves = year - anneeNaissance;
		String req = "SELECT num, age, nom, no, adresse FROM eleve WHERE age =" + anneeNaissanceEleves + " ";
		// Connexion
		DBAction.DBConnexion();
		// Exécution de la requete 
		DBAction.setRes(DBAction.getStm().executeQuery(req));

		while (DBAction.getRes().next()) {
			// Instanciation de mon objet Eleve
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp � travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setAge(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setNo(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleveAnneeNaissance.add(elevTemp);
		}
		// Fermeture de la connexion
		DBAction.DBClose();
		return listEleveAnneeNaissance;

	}

	/**
	 * @param aucun
	 * @return : la liste de tous les �l�ves .
	 */
	public static ArrayList<Eleve> getAllEleve() throws SQLException {

		ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

		String req = "SELECT num, age, nom, no, adresse FROM eleve ";
		// Connexion
		DBAction.DBConnexion();
		// Exécution de la requete et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));

		while (DBAction.getRes().next()) {
			// Instanciation de mon objet Eleve
			Eleve elevTemp = new Eleve();
			// Creation de l'objet eleveTemp � travers le ResultSet BD
			elevTemp.setNum(DBAction.getRes().getString(1));
			elevTemp.setAge(DBAction.getRes().getInt(2));
			elevTemp.setNom(DBAction.getRes().getString(3));
			elevTemp.setNo(DBAction.getRes().getInt(4));
			elevTemp.setAdresse(DBAction.getRes().getString(5));
			listEleve.add(elevTemp);
		}
		// Fermeture de la connexion
		DBAction.DBClose();
		return listEleve;
	}
	

}