package com.crea.dev2.poo.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.crea.dev2.poo.beans.Livre;
import com.crea.dev2.poo.utils.DBAction;

public class LivreDAO {

	/**
	 * Affiche le livre selon la cote passée en paramètre de la fonction.
	 * 
	 * @param cote
	 * @return : retourne l'objet livreTemp
	 * @throws SQLException
	 */
	public static Livre getLivreByCote(String cote) throws SQLException {
		Livre livreTemp = new Livre();
		// ResultSet res ;
		String req = "SELECT * FROM livre WHERE cote = '" + cote + "' ";
		// connexion
		DBAction.DBConnexion();// System.out.println(req);
		// Exécution de la requete et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			// Creation de l'objet livreTemp à travers le ResultSet BD
			livreTemp.setNum(DBAction.getRes().getString(1));
			livreTemp.setTitre(DBAction.getRes().getString(2));
			livreTemp.setDatepret(DBAction.getRes().getDate(3));
			livreTemp.setCote(DBAction.getRes().getString(4));


			livreTemp.affiche();
		}
		// Fermeture de la connexion & statement & res
		// res.close();
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return livreTemp;
	}

	/**
	 * Delete Livre par cote
	 * 
	 * @param cote
	 * @return : 1 ou 0 (le nbr de livres supprimées) sinon le (-) code d'erreur
	 */
	public static int deleteLivreByCote(String cote) {
		int result = -1;

		DBAction.DBConnexion();

		String req = "DELETE FROM livre WHERE cote = \'" + cote + "\'";

		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requête executée");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		System.out.println("[" + req + "] Suppression : Valeur de result == " + result);

		DBAction.DBClose();
		return result;
	}

	/**
	 * Mise à jour du titre du livre par son numero du cote
	 * 
	 * @param No
	 * @return : 1 ou 0 (le nbr de livres mis à jour) sinon le (-) code d'erreur
	 */
	public static int updTitreLivreByCote(String cote, String titre) {
		int result = -1;
		DBAction.DBConnexion();

		String req = "UPDATE livre SET titre = '" + titre + "'  WHERE cote = \'" + cote + "\'";

		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requête executee");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
		}

		DBAction.DBClose();
		return result;
	}

	/**
	 * Ajouter un livre
	 * 
	 * @param String cote, String num, String titre, Date datepret
	 * @return : 1 ou 0 (le nbr des livres) sinon le (-) code d'erreur
	 */
	public static int addLivre(String cote, String num, String titre, Date datepret) {
		int result = -1;
		DBAction.DBConnexion();

		String req = "INSERT INTO livre(cote, num, titre, datepret)" + "VALUES ('" + cote + "','" + num + "','" + titre
				+ "','" + datepret + "')";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requête executee");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
		}

		DBAction.DBClose();
		return result;
	}

	/**
	 * @param aucun
	 * @return : la liste de tous les livres .
	 */
	public static ArrayList<Livre> getAllLivre() throws SQLException {

		ArrayList<Livre> listLivre = new ArrayList<Livre>();

		String req = "SELECT * FROM livre ";

		// Connexion
		DBAction.DBConnexion();

		// exécution de la requète 
		DBAction.setRes(DBAction.getStm().executeQuery(req));

		while (DBAction.getRes().next()) {
			// Instanciation de livre
			Livre livreTemp = new Livre();

			livreTemp.setNum(DBAction.getRes().getString(1));
			livreTemp.setTitre(DBAction.getRes().getString(2));
			livreTemp.setDatepret(DBAction.getRes().getDate(3));
			livreTemp.setCote(DBAction.getRes().getString(4));

			listLivre.add(livreTemp);
		}

		// Fermeture de la connexion
		DBAction.DBClose();
		return listLivre;
	}

}
