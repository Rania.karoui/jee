package com.crea.dev2.poo.dao;

import java.sql.SQLException;

import com.crea.dev2.poo.beans.Livre;
import com.crea.dev2.poo.beans.UV;
import com.crea.dev2.poo.utils.DBAction;

//TO DO
public class UVDAO {
	/**
	 * Affiche l'uv selon le code passée en paramètre de la fonction.
	 * 
	 * @param code
	 * @return : retourne l'objet uvTemp
	 * @throws SQLException
	 */
	public static UV getUVByCode(String code) throws SQLException {
		UV uvTemp = new UV();
		// ResultSet res ;
		String req = "SELECT * FROM uv WHERE code = '" + code + "' ";
		// connexion
		DBAction.DBConnexion();// System.out.println(req);
		// Exécution de la requete et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			// Creation de l'objet eleveTemp à travers le ResultSet BD
			uvTemp.setCode(DBAction.getRes().getString(1));
			uvTemp.setCoord(DBAction.getRes().getString(2));
			uvTemp.setNbh(DBAction.getRes().getInt(3));


			uvTemp.affiche();
		}
		// Fermeture de la connexion & statement & res
		// res.close();
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return uvTemp;
	}
}
