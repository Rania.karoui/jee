package com.crea.dev2.poo.dao;

import java.sql.SQLException;

import com.crea.dev2.poo.beans.Inscrit;
import com.crea.dev2.poo.utils.DBAction;

//TO DO
public class InscritDAO {

	/**
	 * Affiche l'inscrit selon le code passé en paramètre de la fonction.
	 * 
	 * @param code
	 * @return : retourne l'objet inscritTemp
	 * @throws SQLException
	 */
	public static Inscrit getInscritByCode(String code) throws SQLException {
		Inscrit inscritTemp = new Inscrit();
		// ResultSet res ;
		String req = "SELECT * FROM inscrit WHERE code = '" + code + "' ";
		// Connexion
		DBAction.DBConnexion();// System.out.println(req);
		// Exécution de la requete 
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			// Creation de l'objet eleveTemp à travers le ResultSet BD
			inscritTemp.setNum(DBAction.getRes().getString(1));
			inscritTemp.setCode(DBAction.getRes().getString(2));
			inscritTemp.setNote(DBAction.getRes().getFloat(3));
			inscritTemp.affiche();
		}
		// Fermeture de la connexion & statement & res
		// res.close();
		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return inscritTemp;
	}

}
