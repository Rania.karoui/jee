package com.crea.dev2.poo.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.crea.dev2.poo.beans.Chambre;
import com.crea.dev2.poo.utils.DBAction;

//TO DO
public class ChambreDAO {

	/**
	 * Affiche la chambre selon le no passé en paramètre de la fonction.
	 * 
	 * @param no
	 * @return : retourne l'objet chambreTemp
	 * @throws SQLException
	 */
	public static Chambre getChambreByNo(int no) throws SQLException {
		Chambre chambreTemp = new Chambre();
		// ResultSet res ;
		String req = "SELECT * FROM chambre WHERE no = '" + no + "' ";
		
		// Connexion
		DBAction.DBConnexion();

		// Exécution de la requete et init
		DBAction.setRes(DBAction.getStm().executeQuery(req));
		while (DBAction.getRes().next()) {
			// Creation de l'objet chambreTemp à travers le ResultSet BD
			chambreTemp.setNo(DBAction.getRes().getInt(1));
			chambreTemp.setNum(DBAction.getRes().getString(2));
			chambreTemp.setPrix(DBAction.getRes().getFloat(3));
			chambreTemp.affiche();
		}

		DBAction.DBClose();
		// Retourner l'objet ElevTemp
		return chambreTemp;
	}

	/**
	 * Delete Chambre par un numéro
	 * 
	 * @param no
	 * @return : 1 ou 0 (le nbr de chambres supprimées) sinon le (-) code d'erreur
	 */
	public static int deleteChambreByNo(int no) {
		int result = -1;

		DBAction.DBConnexion();

		String req = "DELETE FROM chambre WHERE no = \'" + no + "\'";

		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requête executée");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
			System.out.println(ex.getMessage());
		}
		System.out.println("[" + req + "] Suppression : Valeur de result == " + result);

		DBAction.DBClose();
		return result;
	}

	/**
	 * Mise à jour le prix de la chambre par son no
	 * 
	 * @param No
	 * @return : 1 ou 0 (le nbr de chambres mis à jour) sinon le (-) code d'erreur
	 */
	public static int updPrixChambreByNo(int no, float prix) {
		int result = -1;
		DBAction.DBConnexion();

		String req = "UPDATE chambre SET prix = '" + prix + "'  WHERE no = \'" + no + "\'";

		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requête executee");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
		}

		DBAction.DBClose();
		return result;
	}

	/**
	 * Ajouter une chambre
	 * 
	 * @param int no, String num, float prix
	 * @return : 1 ou 0 (le nbr des chambres) sinon le (-) code d'erreur
	 */
	public static int addChambre(int no, String num, float prix ) {
		int result = -1;
		DBAction.DBConnexion();

		String req = "INSERT INTO chambre(no, prix, num)" + "VALUES ('" + no + "',NULL,'" + prix + "')";
		try {
			result = DBAction.getStm().executeUpdate(req);
			System.out.println("Requête executee");
		} catch (SQLException ex) {
			result = -ex.getErrorCode();
		}

		DBAction.DBClose();
		return result;
	}

	/**
	 * @param aucun
	 * @return : la liste de toutes les chambres .
	 */
	public static ArrayList<Chambre> getAllChambre() throws SQLException {

		ArrayList<Chambre> listChambre = new ArrayList<Chambre>();

		String req = "SELECT * FROM chambre ";

		// Connexion
		DBAction.DBConnexion();

		// Exécution de la requète 
		DBAction.setRes(DBAction.getStm().executeQuery(req));

		while (DBAction.getRes().next()) {
			// Instanciation de Chambre
			Chambre chambTemp = new Chambre();

			chambTemp.setNo(DBAction.getRes().getInt(1));
			chambTemp.setPrix(DBAction.getRes().getFloat(2));
			chambTemp.setNum(DBAction.getRes().getString(3));


			listChambre.add(chambTemp);
		}

		// Fermeture de la connexion
		DBAction.DBClose();
		return listChambre;
	}
}
