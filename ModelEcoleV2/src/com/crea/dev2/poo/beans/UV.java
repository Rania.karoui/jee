package com.crea.dev2.poo.beans;

/**
 * 
 * @author Taha RIDENE Classe UV
 */

public class UV {
	private String code;
	private int nbh;
	private String coord;
	private String num;

	public UV(String code, int nbh, String coord, String num) {
		this.code = code;
		this.nbh = nbh;
		this.coord = coord;
		this.num = num;
	}

	public UV() {
		this("", 0, "", "");
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getNbh() {
		return nbh;
	}

	public void setNbh(int nbh) {
		this.nbh = nbh;
	}

	public String getCoord() {
		return coord;
	}

	public void setCoord(String coord) {
		this.coord = coord;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public void affiche() {
		System.out.println("Code : [" + this.code + "] Nombre l'heure de cours: [" + this.nbh + "]  Coord : ["
				+ this.coord + "] Numéro de l'elève: [" + this.num + "]  ");
	}
}
