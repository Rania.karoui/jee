package com.crea.dev2.poo.beans;

import java.util.Date;

/**
 * 
 * @author Taha RIDENE Classe Livre
 */

public class Livre {
	private String titre;
	private Date datepret;
	private String cote;
	private String num;

	public Livre(String cote, String num, String titre, Date datepret) {
		this.cote = cote;
		this.num = num;
		this.titre = titre;
		this.datepret = datepret;
	}

	public Livre() {
		this("", "", "", null);
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Date getDatepret() {
		return datepret;
	}

	public void setDatepret(Date datepret) {
		this.datepret = datepret;
	}
	
	
	public String getCote() {
		return cote;
	}

	public void setCote(String cote) {
		this.cote = cote;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}


	public void affiche() {
		System.out.println("Cote : [" + this.cote + "] Numéro de l'elève: [" + this.num + "]  Titre du Livre : ["
				+ this.titre + "] Date du prêt : [" + this.datepret + "]  ");
	}
}
