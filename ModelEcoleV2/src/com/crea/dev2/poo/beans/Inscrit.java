package com.crea.dev2.poo.beans;

/**
 * 
 * @author Taha RIDENE Classe Inscrit
 */
public class Inscrit {
	private String code;
	private float note;
	private String num;

	public Inscrit(String code, String num, float note) {

		this.code = code;
		this.note = note;
		this.num = num;
	}

	public Inscrit() {
		this("", "", 0);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
	public float getNote() {
		return note;
	}
	
	public void setNote(float note) {
		this.note = note;
	}
	
	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}


	public void affiche() {
		System.out.println("Code : [" + this.code + "] Numéro de l'elève: [" + this.num + "]  Note de l'élève :["
				+ this.note + "]  ");
	}

}
